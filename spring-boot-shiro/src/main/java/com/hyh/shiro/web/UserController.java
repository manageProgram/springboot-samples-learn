package com.hyh.shiro.web;

import com.hyh.shiro.common.AjaxResult;
import com.hyh.shiro.service.UserService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Summerday
 */

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserService userService;

    @GetMapping
    public AjaxResult get() {
        return AjaxResult.ok("get!");
    }

    /**
     * RequiresRoles 是所需角色 包含 AND 和 OR 两种
     * RequiresPermissions 是所需权限 包含 AND 和 OR 两种
     *
     * @return msg
     */

    @RequiresRoles(value = {"admin"})
    //@RequiresPermissions(value = {"user:list", "user:query"}, logical = Logical.OR)
    @GetMapping("/rq_admin")
    public AjaxResult requireRoles() {
        return AjaxResult.ok("result : admin");
    }

    @RequiresPermissions(value = {"edit", "view"}, logical = Logical.AND)
    @GetMapping("/rq_edit_and_view")
    public AjaxResult requirePermissions() {
        return AjaxResult.ok("result : edit + view");
    }


}
