package com.hyh.validator.web;

import com.hyh.validator.entity.Person;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Summerday
 */

@RestController
public class CommonController {

    @PostMapping("/form")
    public String form(@RequestBody Person person) {
        if (person.getName() == null) {
            return "姓名不能为null";
        }
        if (person.getName().length() < 6 || person.getName().length() > 12) {
            return "姓名长度必须在6 - 12之间";
        }
        if (person.getAge() == null) {
            return "年龄不能为null";
        }
        if (person.getAge() < 20) {
            return "年龄最小需要20";
        }
        // service ..
        return "注册成功!";
    }

}
