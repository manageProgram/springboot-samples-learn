package com.hyh.simple.observe;

/**
 * 观察者接口
 * @author Summerday
 */
public interface Observer {

    void update(String message);
}
