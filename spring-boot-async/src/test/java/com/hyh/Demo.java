package com.hyh;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Summerday
 */
public class Demo {


    public static void main(String[] args) {

        // 不可靠: 队列和线程池中的任务仅仅存储在内存中，如果 JVM 进程被异常关闭，将会导致丢失，未被执行。[一定要保证关闭前执行完所有的任务]
        // 可靠: 分布式消息队列,异步调用会以一个消息的形式，存储在消息队列的服务器上，所以即使 JVM 进程被异常关闭，消息依然在消息队列的服务器上。
        ExecutorService executor = Executors.newFixedThreadPool(10);
        executor.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("异步调用!!!");
            }
        });
    }
}
