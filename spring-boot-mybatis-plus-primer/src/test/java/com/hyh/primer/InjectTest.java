package com.hyh.primer;

import com.hyh.primer.entity.User;
import com.hyh.primer.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
class InjectTest {

    @Resource
    UserMapper userMapper;

    @Test
    void inject(){
        int rows = userMapper.deleteAll();
        System.out.println(rows);
    }

    @Test
    void findOne(){
        User one = userMapper.findOne(1L);
        System.out.println(one);
    }

    @Test
    void insertBatch(){
        User user1 = new User();
        user1.setManagerId(1088248166370832385L);
        user1.setName("hyh1");
        user1.setAge(30);

        User user2 = new User();
        user2.setManagerId(1088248166370832385L);
        user2.setName("summer1");
        List<User> users = Arrays.asList(user1, user2);
        int i = userMapper.insertBatchSomeColumn(users);
        System.out.println(i);

    }

    @Test
    void LogicDeleteByIdWithFill(){
        User user = new User();
        user.setId(1320588722978160642L);
        // 字段需要需使用@TableField(fill=FieldFill.UPDATE)
        user.setAge(30);
        int row = userMapper.deleteByIdWithFill(user);
        System.out.println(row);
    }

    @Test
    void AlwaysUpdateSomeColumnById(){
        User user = new User();
        user.setAge(16);
        user.setId(1320588722978160642L);
        user.setName("xxx");
        int i = userMapper.alwaysUpdateSomeColumnById(user);
        System.out.println(i);
    }
}
