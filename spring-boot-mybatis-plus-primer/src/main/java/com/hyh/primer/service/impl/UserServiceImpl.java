package com.hyh.primer.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hyh.primer.entity.User;
import com.hyh.primer.mapper.UserMapper;
import com.hyh.primer.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Wrapper;
import java.util.List;

/**
 * Service实现类,继承ServiceImpl,实现接口
 * @author Summerday
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    UserMapper mapper;

    @Override
    public List<User> mySelectList() {
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        return mapper.mySelectList(wrapper.gt("age",25));
    }
}
