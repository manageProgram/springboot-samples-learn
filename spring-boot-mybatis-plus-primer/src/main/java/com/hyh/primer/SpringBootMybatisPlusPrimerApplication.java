package com.hyh.primer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMybatisPlusPrimerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMybatisPlusPrimerApplication.class, args);
    }

}
