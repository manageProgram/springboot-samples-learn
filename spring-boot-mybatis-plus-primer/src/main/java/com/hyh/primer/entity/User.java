package com.hyh.primer.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Summerday
 */
@Data
@ToString
@TableName("user") //指定表名
@EqualsAndHashCode(callSuper = false)
public class User extends Model<User> {

    @TableId(type = IdType.ASSIGN_ID) //指定主键
    private Long id;

    @TableField(condition = SqlCondition.LIKE) //指定字段名
    private String name;
    private String email;
    private Integer age;

    // 上级id
    private Long managerId;

    // 创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    // 更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    // 版本号
    @Version
    private Integer version;

    // 逻辑删除字段
    //@TableLogic
    @TableField(select = false)
    private Integer deleted;

}
