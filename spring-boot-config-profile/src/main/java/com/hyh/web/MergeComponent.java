package com.hyh.web;

import com.hyh.config.MergeProperties;
import com.hyh.config.MyProperties;
import com.sun.org.apache.bcel.internal.generic.MethodGen;
import com.sun.org.apache.xpath.internal.SourceTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.net.idn.Punycode;

import javax.annotation.PostConstruct;

/**
 * @author Summerday
 */

@Component
public class MergeComponent {

    private final MergeProperties mergeProperties;

    @Autowired
    public MergeComponent(MergeProperties mergeProperties) {
        this.mergeProperties = mergeProperties;
    }

    @PostConstruct
    public void init() {
        System.out.println(mergeProperties);
    }
}
