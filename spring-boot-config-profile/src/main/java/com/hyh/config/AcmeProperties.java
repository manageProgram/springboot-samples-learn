package com.hyh.config;

import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.net.InetAddress;
import java.util.List;

@ToString
@ConstructorBinding
@ConfigurationProperties("acme")
public class AcmeProperties {

    private final Security security;
    private final boolean enabled;

    private final InetAddress remoteAddress;

    public AcmeProperties(boolean enabled, InetAddress remoteAddress, @DefaultValue Security security) {
        this.enabled = enabled;
        this.remoteAddress = remoteAddress;
        this.security = security;
    }

    public boolean isEnabled() {
        return enabled;
    }


    public InetAddress getRemoteAddress() {
        return remoteAddress;
    }


    public Security getSecurity() {
        return security;
    }

    @ToString
    public static class Security {

        private final String username;

        private final String password;

        private final List<String> roles;


        public Security(String username, String password,
                        @DefaultValue("USER") List<String> roles) {
            this.username = username;
            this.password = password;
            this.roles = roles;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

        public List<String> getRoles() {
            return roles;
        }
    }
}