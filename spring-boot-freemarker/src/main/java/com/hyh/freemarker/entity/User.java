package com.hyh.freemarker.entity;

import lombok.Data;

/**
 * @author Summerday
 */
@Data
public class User {

    private String username;
    private Integer id;
}
